class Character < ApplicationRecord
    has_and_belongs_to_many :movies
    has_one_attached :image

    validates :name, :history, :image, :age, :weight, presence: true
    validates :name, uniqueness: true
end
