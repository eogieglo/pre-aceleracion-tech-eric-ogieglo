class Movie < ApplicationRecord
    validates :title, :image, presence: true
    validates :title, uniqueness: true
    has_one_attached :image

    has_and_belongs_to_many :characters
    has_many :genders
end
