class CreateMovies < ActiveRecord::Migration[6.1]
  def change
    create_table :movies do |t|
      t.string :image
      t.string :title
      t.date :create_date
      t.integer :qualification
      t.string :character_movie

      t.timestamps
    end
  end
end
