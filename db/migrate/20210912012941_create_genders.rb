class CreateGenders < ActiveRecord::Migration[6.1]
  def change
    create_table :genders do |t|
      t.string :image
      t.string :name
      t.string :movie

      t.timestamps
    end
  end
end
